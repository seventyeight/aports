# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=difftastic
pkgver=0.34.0
pkgrel=0
pkgdesc="Diff tool that understands syntax"
url="https://difftastic.wilfred.me.uk/"
license="MIT"
arch="all !s390x !riscv64" # blocked by rust/cargo
arch="$arch !aarch64 !armhf !armv7" # FTBFS on arm
makedepends="cargo openssl-dev>3"
source="https://github.com/Wilfred/difftastic/archive/$pkgver/difftastic-$pkgver.tar.gz"

prepare() {
	default_prepare
	cargo fetch --locked
}

build() {
	cargo build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/difft -t "$pkgdir"/usr/bin
}

sha512sums="
b394d6cec02eaf9adaab4780639f1a93a8709aa2e4d9d83b526c17dcac709c38789974fac9d4c56e39f558c20b496059ea468993ffba3571283fba61114e4a5b  difftastic-0.34.0.tar.gz
"
